# == Class: hosts_windows: copys a default host file to a windows host, filled via samrat variabls in foreman 
class hosts_windows(
  Hash $hostentrys = {'127.0.0.1' => 'localhost', '192.168.56.12' => 'foreman.loc.heimhub.net','::1' => 'localhost'}
) {
  if $osfamily == 'windows' {
    file { 'C:\Windows\System32\drivers\etc\hosts':
            owner   => 'SYSTEM',
            content => template('hosts_windows/hosts_windows.erb'),
            replace => true,
        }
  }
}
