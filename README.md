# hosts_windows 
*careful! Quick and dirty solution.*

Class description:

Manages the ‘hosts’ file on Windows Systems.

It writes the entries provided within the variable to
‘C:\\Windows\\System32\\drivers\\etc\\hosts’ and sets its owner to
‘SYSTEM’.


### Tested on:

-   Windows Server 2008R2
-   might work within the Windows Family\
    (and yes, it’s the IT might work)


### Class dependency’s:

Puppet &gt;= 5.0


### CVS:

[*https://gitlab.com/e.heim/windows-hostfile*](https://gitlab.com/e.heim/windows-hostfile)


### Class variables:

-   **Hostentrys**

    -   type:\
        ruby hash
    -   description:\
        Entries to write to the hostsfile.
    -   Foreman is able to overwrite it
    -   *example*:\
        '127.0.0.1' =&gt; 'localhost',\
        '192.168.56.12' =&gt; 'foreman.[]{#__DdeLink__60_4173842489
        .anchor}googlrhub.net',
    -   *foreman example*:
	'127.0.0.1': 'localhost',\
	'192.168.56.12': 'foreman.googlrhub.net',

### ToDo’s:

-   unit tests
-   integration tests
-   variable validations


### useful links:

-   [*puppet 101*](https://blogs.sequoiainc.com/puppet-101-part-1/)
-   [*puppet 5 Type Reference*](https://puppet.com/docs/puppet/5.5/type.html)


